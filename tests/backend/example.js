const baseURL = require('../index').baseURL;
const pdsURL = require('../index').pdsURl;
const request = require('supertest')(baseURL);
const pdsRequest =require('supertest')(pdsURL);
const assert = require('chai').assert;
const expect = require("chai").expect;
const moment = require('moment');

var today = new Date();
var date = moment(today).format('YYYY-MM-DD');

describe('event data endpoint', () => {

    let body = request.get('/inplay/en-gb/OB_SP9?marketType=match-betting-live&sortKey=competition');

    it('example test', () => {
        return body
            .expect(200)
            .then((response) => {
                assert.isNotEmpty(response.body);
            });
    });

    it('return correct event with selections data on the endpoint /inplay/en-gb/OB_SP9?marketType=match-betting-live&sortKey=competition', async () => {
        return body
            .then((response) => {
                assert.typeOf(response.body.sports, 'array');
                assert.typeOf(response.body.sports[0], 'object');
            });
    });

    it('equal /inplay/en-gb/OB_SP9?marketType=match-betting-live&sortKey=competition', async () => {
        return body
            .then((response) => {
                expect(Object.keys(response.body.sports[0])).to.eql([
                    "name",
                    "translatedName",
                    "id",
                    "eventCount"
                ]);
            });
    });
});

describe('pds data endpoint', () => {

    it('example test', () => {
        let body = pdsRequest.get(`/events?dateFrom=${date}T00:00:00&dateTo=${date}T23:59:00`);

        return body
            .then((response) => {
                let bodyRequest = response.body;
                assert.isNotEmpty(bodyRequest);

                console.log(bodyRequest);
                let eventList = bodyRequest.map(e => e.id);
                console.log("================");
                console.log(eventList);
                console.log("================");
                let eventBody = request.get(`/${eventList[0]}`);
                console.log(eventBody);

                return eventBody
                    .expect(200)
                    .then((response) => {
                        assert.isNotEmpty(response.body);
                    });
            });
    });
});
