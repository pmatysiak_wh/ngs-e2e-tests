const env = process.env.NODE_ENV === 'prod' ? '' : `-${process.env.NODE_ENV}`;

const baseURL = `https://sports.williamhill${env}.com/data/ngs`;

const pdsURl = 'http://ctgdata.williamhill-pp1.com/pds2/catalogs/OB_CG0/classes/OB_CL2';

module.exports = { baseURL, pdsURl };
